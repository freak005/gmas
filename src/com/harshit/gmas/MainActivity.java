package com.harshit.gmas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	EditText usrnme, psswd;
	TextView tt;
	Button login;
	String string_un, string_pw;
	Cookie getCookie;
	String endResult, sessionidvalue, csrfvalue;
	String cookieString;
	Editor editor;
	String result;
	List<Cookie> cookies;
	HttpResponse end;
	public static String username;
	String url = "http://www.getmeashop.com/accounts/login/";
	String totals;
	ProgressDialog progressBar;
	AlertDialog alertDialog = null;
	private static String getCSRFtoken;
	WebView webview;
	RelativeLayout rl;
	WebView WebV;
	SharedPreferences prefs;
	 Map<String, String> abc;
	Context currentContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		prefs = this.getSharedPreferences(
			      "com.harshit.gmas", Context.MODE_PRIVATE);

		setContentView(R.layout.activity_main);
		currentContext = this;
		tt = (TextView)findViewById(R.id.textView1);
		usrnme = (EditText) findViewById(R.id.username);
		alertDialog = new AlertDialog.Builder(this).create();
		psswd = (EditText) findViewById(R.id.password);
		login = (Button) findViewById(R.id.login);
		webview = (WebView) findViewById(R.id.wbv1);
		
		if(prefs.contains("cookiestring"))
		{	Log.d("sharedpref","sharedpref");
			string_un = prefs.getString("string_un", "");
			string_pw = prefs.getString("string_pw", "");

			new CheckAsyncTask()
					.execute("http://www.getmeashop.com/accounts/login/");
			
			usrnme.setVisibility(View.INVISIBLE);
			tt.setVisibility(View.INVISIBLE);
			psswd.setVisibility(View.INVISIBLE);
			login.setVisibility(View.INVISIBLE);
		}
		
        CookieSyncManager.createInstance(this); 
        CookieSyncManager.getInstance().startSync();

		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				string_un = usrnme.getText().toString();
				string_pw = psswd.getText().toString();
				username = string_un;
				new CheckAsyncTask()
						.execute("http://www.getmeashop.com/accounts/login/");

			}
		});

	}

//	@Override
//	protected void onRestart() {
//		// TODO Auto-generated method stub
//		super.onRestart();
//	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

        CookieSyncManager.getInstance().sync();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

        CookieSyncManager.getInstance().stopSync();


	}
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode)
            {
            case KeyEvent.KEYCODE_BACK:
                if(WebV.canGoBack()){
                    WebV.goBack();
                }else{
                    finish();
                }
                return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

	private String sendContacts() {
		try {
			// Create the HTTP request

			Cookie cookie = null;
			HttpParams httpParameters = new BasicHttpParams();

			// Setup timeouts
			HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
			HttpConnectionParams.setSoTimeout(httpParameters, 15000);

			HttpClient httpclient = new DefaultHttpClient(httpParameters);
			if (cookies != null) {
				int size = cookies.size();
				for (int i = 0; i < size; i++) {
					((AbstractHttpClient) httpclient).getCookieStore()
							.addCookie(cookies.get(i));
				}
			}

			HttpPost httpPost = new HttpPost(
					"http://www.getmeashop.com/accounts/login/");
			httpPost.addHeader("Host", "www.getmeashop.com");
			httpPost.addHeader("User-Agent", "Mozilla/5.0");
			httpPost.addHeader("Accept",
					"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpPost.addHeader("Accept-Language", "en-US,en;q=0.5");
			httpPost.addHeader("Accept-Encoding", "gzip, deflate");
			httpPost.addHeader("Referer", "http://www.getmeashop.com/home/");
			httpPost.addHeader("Cookie", "csrftoken=" + getCSRFtoken);
			// ma=217405696.1532962016.1403170396.1403170396.1403170396.1;
			// __utmb=217405696.1.10.1403170396; __utmc=217405696;
			// __utmz=217405696.1403170396.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)");
			httpPost.addHeader("Connection", "keep-alive");
			httpPost.addHeader("Content-Type",
					"application/x-www-form-urlencoded");
			// httpPost.addHeader("Content-Length", "93");

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("csrfmiddlewaretoken",
					getCSRFtoken));
			nameValuePairs.add(new BasicNameValuePair("username", string_un));
			nameValuePairs.add(new BasicNameValuePair("password", string_pw));

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			BasicCookieStore cookieStore = new BasicCookieStore();

			cookieStore.addCookie(getCookie);

			System.out.println("cookies prepared");
			HttpContext localContext = new BasicHttpContext();
			localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

			HttpResponse httpResponse = httpclient.execute(httpPost);
			end = httpResponse;
			List<Cookie> cookies = ((AbstractHttpClient) httpclient)
					.getCookieStore().getCookies();


			System.out.println("size of cookies : " + cookies.size());
			for (int i = 0; i < cookies.size(); i++) {
				Log.d(cookies.get(i).getName(), cookies.get(i).getValue());
				if (cookies.get(i).getName().equals("sessionid")) {
					sessionidvalue = cookies.get(i).getValue();
					System.out.println("Session + htmlStirng" + sessionidvalue);
				}
				if (cookies.get(i).getName().equals("csrftoken")) {
					csrfvalue = cookies.get(i).getValue();
					System.out.println("Session + htmlStirng" + csrfvalue);
				}
		         abc = new HashMap<String, String>();
		         abc.put("sessionid", sessionidvalue);
		         abc.put("csrftoken", csrfvalue);

				cookie = cookies.get(i);
			}
			Cookie sessionCookie = cookie;
			CookieSyncManager.createInstance(this);
			CookieManager cookieManager = CookieManager.getInstance();
			 for (Cookie cook : cookies){
                 
	                sessionCookie = cook;
	     
	                    cookieString = sessionCookie.getName() + "=" + sessionCookie.getValue() + "; domain=" + sessionCookie.getDomain();
	                    cookieManager.setCookie("http://www.getmeashop.com/accounts/login/", cookieString);
	                    CookieSyncManager.getInstance().sync();
	            }
			 editor = prefs.edit();
	            editor.putString("cookiestring", cookieString);
	            editor.putString("string_un", string_un);
	            Log.d("sharedpref", "storing values in editor");
	            editor.putString("total", totals);
	            editor.putString("string_pw", string_pw);

	            editor.commit(); 

			// int responseCode= httpResponse.getStatusLine().getStatusCode();
			// System.out.println("Response Code : " + responseCode);

			InputStream inputStream = httpResponse.getEntity().getContent();
			//
			// result = convertInputStreamToString(inputStream);
			// System.out.println("the response is :"+result);
			String line = "";
			StringBuilder total = new StringBuilder();
			// Wrap a BufferedReader around the InputStream
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					inputStream));
			// Read response until the end
			try {
				while ((line = rd.readLine()) != null) {
					total.append(line);
					totals = total.toString();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			System.out.println("ERROR in post : " + e);
			// Log.d("Exception", e.getLocalizedMessage());
		}

		endResult = result;
		// Intent myWebViewIntent = new Intent(MainActivity.this,
		// MyWebViewActivity.class);
		// myWebViewIntent.putExtra("session", sessionidvalue);
		// myWebViewIntent.putExtra("csrf", csrfvalue);
		// myWebViewIntent.putExtra("htmlstring", totals);
		// // myWebViewIntent.putExtra("username", endResult);
		// startActivity(myWebViewIntent);
		return "successful";
	}

	@SuppressLint("SetJavaScriptEnabled")
	private class HttpAsyncTask extends AsyncTask<String, Void, String> {
		// @Override
		protected String doInBackground(String... urls) {
			return sendContacts();
		}

		// onPostExecute displays the results of the AsyncTask.
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String result) {
			WebV = new WebView(currentContext);
			CookieSyncManager syncManager = CookieSyncManager.createInstance(WebV.getContext());
			CookieManager cookieManager = CookieManager.getInstance();
			

			cookieManager.setCookie("http://www.getmeashop.com/accounts/login/", cookieString); // Here your cookie
			
			syncManager.sync();

			final WebSettings settings = WebV.getSettings();
			settings.setJavaScriptEnabled(true);
			settings.setAppCacheEnabled(true);
			settings.setBuiltInZoomControls(true);
			settings.setPluginState(WebSettings.PluginState.ON);
			WebV.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

			WebV.setWebViewClient(new WebViewClient() {
				
				    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				        //Users will be notified in case there's an error (i.e. no internet connection)
				        Toast.makeText(currentContext, "Oh no! " + description, Toast.LENGTH_SHORT).show();
				    }

				    public void onPageFinished(WebView view, String url) {
				        CookieSyncManager.getInstance().sync();
				    } 
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					// return super.shouldOverrideUrlLoading(view, url);
				//	 view.loadUrl(url);
					return false;
				}
			});
			setContentView(WebV);
//			WebV.loadDataWithBaseURL("http://www.getmeashop.com/" + string_un
//					+ "/dashboard/", totals, "text/html", "utf-8",
//					"http://www.getmeashop.com/accounts/login/");
			WebV.loadUrl("http://www.getmeashop.com/" + string_un
					+ "/dashboard/", abc);

		}

	}

	private String getCheckList() {
		try {
			// Create the HTTP request
			// HttpParams httpParameters = new BasicHttpParams();

			// Setup timeouts
			// HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
			// HttpConnectionParams.setSoTimeout(httpParameters, 15000);

			// HttpClient httpclient = new DefaultHttpClient(httpParameters);
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet request = new HttpGet();
			request.setURI(URI
					.create("http://www.getmeashop.com/accounts/login/"));
			HttpResponse response = httpclient.execute(request);
			System.out.println("The GET status code: "
					+ response.getStatusLine().getStatusCode());
			cookies = ((AbstractHttpClient) httpclient).getCookieStore()
					.getCookies();

			getCookie = cookies.get(0);
			System.out.println("size of cookies : " + cookies.size());
			getCSRFtoken = cookies.get(0).getValue();

			Log.d("get-cookie", cookies.get(0).getName() + ":"
					+ cookies.get(0).getValue());

		} catch (Exception e) {
			System.out.println("ERROR in get : " + e);
			Log.d("Exception", e.getLocalizedMessage());
		}
		return "successful";
	}

	private class CheckAsyncTask extends AsyncTask<String, Void, String> {
		// @Override
		protected String doInBackground(String... urls) {

			return getCheckList();
		}

		@Override
		protected void onPostExecute(String result) {
			new HttpAsyncTask()
					.execute("http://www.getmeashop.com/accounts/login/");
		}
	}

	public String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null) {
			result += line;
			// System.out.println("---->"+line);
		}

		inputStream.close();
		return result;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	

}

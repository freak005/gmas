package com.harshit.gmas;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MyWebViewActivity extends Activity {
	ProgressDialog progressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
	    WebView webview = (WebView)findViewById(R.id.wbv);

	    Bundle extras = getIntent().getExtras();
	    if(extras != null) {

	         // Get endResult
	         String sessionstring = extras.getString("session");
	         String csrfstring = extras.getString("csrf");
	         String htmlString = extras.getString("htmlstring");
	         //	webview.loadData(htmlString, "text/html", "utf-8");
	         // webview.loadUrl(http://www.getmeashop.com/freak005/dashboard/);
	         final WebSettings settings = webview.getSettings();
	         settings.setJavaScriptEnabled(true);
	         settings.setAppCacheEnabled(true);
	         settings.setBuiltInZoomControls(true);
	         settings.setPluginState(WebSettings.PluginState.ON);
	         Map<String, String> abc = new HashMap<String, String>();
	         abc.put("sessionid", sessionstring);
	         abc.put("csrftoken", csrfstring);

	         webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

	         final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

	         progressBar = ProgressDialog.show(MyWebViewActivity.this, "GMaS", "Loading...");

	         webview.setWebViewClient(new WebViewClient() {
	             public boolean shouldOverrideUrlLoading(WebView view, String url) {
	                 Log.i("Tag", "Processing webview url click...");
	                 view.loadUrl(url);
	                 return true;
	             }

	             public void onPageFinished(WebView view, String url) {
	                 Log.i("Tag", "Finished loading URL: " +url);
	                 if (progressBar.isShowing()) {
	                     progressBar.dismiss();
	                 }
	             }

	             public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
	                 Log.e("Tag", "Error: " + description);
	      //           Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
	                 alertDialog.setTitle("Error");
	                 alertDialog.setMessage(description);
	                 alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
	                     public void onClick(DialogInterface dialog, int which) {
	                         return;
	                     }
	                 });
	                 alertDialog.show();
	             }
	         });
	      //   webview.loadUrl("http://www.getmeashop.org/"+MainActivity.username+"/dashboard/", abc);
	     //    webview.loadUrl("http://www.getmeashop.org/harshit/dashboard/", abc);
	         webview.loadData(htmlString, "text/html", "utf-8");
	    }
	}
	
}
